// Setup express
const express = require("express")
const router = express.Router()

// Setup markdown
const fs = require("fs")
const md = require("markdown-it")()

// Setup dirtree
const dirtree = require("directory-tree")


// Blog index
router.get("/", (req, res) => {

    // Get pages
    const fileStructure = dirtree("./blog")
    let blogPages = []
    let blogPageTitles = []
    
    // Year
    for (y = 0; y < fileStructure.children.length; y++) {
        let fileYear = fileStructure.children[y]

        // Month
        for (m = 0; m < fileYear.children.length; m++) {
            let fileMonth = fileYear.children[m]

            // Day
            for (d = 0; d < fileMonth.children.length; d++) {

                // Dir
                blogPages.push(fileMonth.children[d].path)

                // Titles
                let title = fs.readFileSync("./" + fileMonth.children[d].path)
                blogPageTitles.push(title.toString().split("\n")[0].substring(2))
            }
        }
    }

    // Render
    res.render("blog-index", {title: "Blog", posts: blogPages, titles: blogPageTitles})
})


// Blog page
router.get("/:year/:month/:day", (req, res) => {

    // Render markdown file
    let path = "./blog" + req.url + ".md"
    fs.readFile(path, "utf-8", (err, data) => {

        // 404
        if(err) {
            res.render("404", {title: "404", wrong: "blog"})
        } else {
            let title = data.toString().split("\n")[0].substring(2)
            res.render("blog", {title: title, data: md.render(data)})
        }
    })
})

// Export
module.exports = router