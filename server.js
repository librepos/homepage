// Setup express
const express = require("express")
const app = express()
const port = 3000

// Setup pug
app.use(express.static("./public"))
app.set("views", "./views")
app.set("view engine", "pug")

// Homepage
app.get("/", (req, res) => {
    res.render("home", {title: "Home"})
})

// About page
app.get("/about", (req, res) => {
    res.render("about", {title: "About"})
})

// Blog router
const blog = require("./routes/blog")
app.use("/blog", blog)

// Run server
app.listen(port, () => {
    console.log("[Server] Running on port " + port)
})